package com.example.codvaapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.codvaapplication.databinding.ActivityMainBinding
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMainBinding

    private var itemPositions = arrayOf("1", "2", "3", "4", "5", "6", "7", "8","")
    private val correctPos = arrayOf("1", "2", "3", "4", "5", "6", "7", "8","")
    private var count = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        count = binding.slidingPuzzleGrid.childCount

        start()
    }


    // Firstly, the buttons are shuffled, then every position is set as text to the buttons.
    // every position is contained in the tag so that we can get the information about button in the future.
    private fun start() {
        randomizeNumbers()
        for (i in 0 until count) {
            val child = binding.slidingPuzzleGrid.getChildAt(i) as Button
            child.text = itemPositions[i]
            child.tag = i
            child.setOnClickListener(this)
        }
    }

    // This function randomizes positions of buttons, basically shuffles them.
    private fun randomizeNumbers(){
        val randomNum = Random()
        for (i in 0 until count){
            val newId = randomNum.nextInt(count)
            val currentId = itemPositions[i]
            itemPositions[i] = itemPositions[newId]
            itemPositions[newId] = currentId
        }
    }

    // On every click we get the position of the button that is clicked then pass it to the slideButton
    // function so that it can slide if it can. We get the position of the empty button by calling the
    // positionOfEmptyButton function which returns what it says.
    override fun onClick(v: View?) {
        val btn: Button = v as Button
        val position = btn.tag as Int
        slideButtons(position, positionOfEmptyButton())
        if (resultCheck()){
            Toast.makeText(this, "Congrats!!", Toast.LENGTH_SHORT).show()
        }
    }

    // As parameters this function has the position of clicked button and the position of the empty button,
    // If the button can move, positions are swapped between the two buttons as well as their texts.
    private fun slideButtons(pos: Int, emptyPos: Int) {
        if (canMove(pos)) {
            // change positions in the array
            val currentPos = itemPositions[pos]
            itemPositions[pos] = itemPositions[emptyPos]
            itemPositions[emptyPos] = currentPos
            // change texts. Bc the buttons are "children" of grid layout, I have to get them like
            // I do below (as Button) otherwise, im not able to set text to it.
            val btn: Button = binding.slidingPuzzleGrid.getChildAt(pos) as Button
            val emptyBtn: Button = binding.slidingPuzzleGrid.getChildAt(emptyPos) as Button
            btn.text = itemPositions[pos]
            emptyBtn.text = itemPositions[emptyPos]
        }
    }

    // This function returns the position of empty button. It goes through the loop to find
    // the correct position and check weather empty button is at that position.
    private fun positionOfEmptyButton(): Int {
        var position = 0
        for (i in 0 until count) {
            position = if (itemPositions[i] == "") i else continue
        }
        return position
    }

    // If the position is from 3 to 8 and on top of them is empty button, it can move up
    // On top of them is empty button when the position minus 3 is the said button.
    private fun canGoUp(pos: Int): Boolean{
        return ((pos in 3..8) && (itemPositions[pos - 3] == ""))
    }

    // If the position is from 0 to 5 and below them is empty button, they can move down.
    // We check weather the button is empty below by subtracting 3 from position.
    private fun canGoDown(pos: Int): Boolean{
        return ((pos in 0..5) && (itemPositions[pos + 3] == ""))
    }

    // If the position is not 2, 5 or 8 (they are at the right edge of the grid), and
    // they have empty button from the right (position plus 1 indicates this), they can move right.
    private fun canGoRight(pos: Int): Boolean{
        return ((pos != 2 && pos != 5 && pos != 8) && (itemPositions[pos + 1] == ""))
    }

    // If the positions are not at the left edge and from their left is the empty button,
    // they can move. Logic is similar to the canGoRight function's logic.
    private fun canGoLeft(pos: Int): Boolean{
        return ((pos != 0 && pos != 3 && pos != 6) && (itemPositions[pos - 1] == ""))
    }

    // If the button can move either up, down, right or left it means it is movable.
    //this function returns a boolean which indicates the statemend below.
    private fun canMove(pos: Int): Boolean {
        return (canGoUp(pos) || canGoDown(pos) || canGoRight(pos) || canGoLeft(pos))
    }

    // This function checks the results by checking if the buttons' positions and the correct positions
    // which are contained in the separate array (correctPos) match. if they match the function returns true,
    // otherwise - false.
    private fun resultCheck(): Boolean {
        for (i in 0 until count) {
            if (itemPositions[i] != correctPos[i]) {
                return false
            }
        }
        return true
    }
}